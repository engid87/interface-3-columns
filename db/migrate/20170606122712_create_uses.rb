class CreateUses < ActiveRecord::Migration[5.1]
  def change
    create_table :uses do |t|
      t.text :description
      t.string :sns

      t.timestamps
    end
  end
end
