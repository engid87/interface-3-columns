class CreateSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :settings do |t|
      t.string :sns
      t.text :name
      t.text :description
      t.text :story
      t.text :default
      t.text :recommended
      t.text :current

      t.timestamps
    end
  end
end
