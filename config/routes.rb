Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html



   get '/', to: 'privacy_setting#index'
   get '/privacy_setting', to: 'privacy_setting#index'
   get '/privacy_setting/show', to: 'privacy_setting#show'
   get '/policy', to: 'policy#index'
   get '/policy/show', to: 'policy#show'
   get '/public', to: 'public#index'
   get '/public/show', to: 'public#show'
   get '/about', to: 'about#index'

end
