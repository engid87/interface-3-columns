class PolicyController < ApplicationController
before_action :set_setting, only: [:show, :edit, :update, :destroy]

# GET /settings
# GET /settings.json
def index
  @post = Post.all
  @lk = Post.where(sns: "lk")
  @fbs = Post.where(sns: "fb")
  @tws = Post.where(sns: "tw")
  @google = Post.where(sns: "g+")
  @pin = Post.where(sns: "pin")

  @collect = Collect.all
  @fbc = Collect.where(sns: "fb")
  @twc = Collect.where(sns: "tw")
  @lkc = Collect.where(sns: "lk")
  @googlec = Collect.where(sns: "google")
  @pinc = Collect.where(sns: "pin")

  @use = Use.all
  @fbu = Use.where(sns: "fb")
  @twu = Use.where(sns: "tw")
  @lku = Use.where(sns: "lk")
  @googleu = Use.where(sns: "google")
  @pinu = Use.where(sns: "pin")

  @sell = Sell.all
  @fbsell = Sell.where(sns: "fb")
  @twsell = Sell.where(sns: "tw")
  @lksell = Sell.where(sns: "lk")
  @googlesell = Sell.where(sns: "google")
  @pinsell = Sell.where(sns: "pin")



end



# GET /settings/1
# GET /settings/1.json
def show
  @lk = Setting.find(params[:id])
  respond_to do |format|
    format.js {render layout: false}
  end
end

# GET /settings/new
def new
  @setting = Setting.new
end


# GET /settings/1/edit
def edit
end

# POST /settings
# POST /settings.json
def create
  @setting = Setting.new(setting_params)

  respond_to do |format|
    if @setting.save
      format.html { redirect_to @setting, notice: 'Setting was successfully created.' }
      format.json { render :show, status: :created, location: @setting }
    else
      format.html { render :new }
      format.json { render json: @setting.errors, status: :unprocessable_entity }
    end
  end
end

# PATCH/PUT /settings/1
# PATCH/PUT /settings/1.json
def update
  respond_to do |format|
    if @setting.update(setting_params)
      format.html { redirect_to @setting, notice: 'Setting was successfully updated.' }
      format.json { render :show, status: :ok, location: @setting }
    else
      format.html { render :edit }
      format.json { render json: @setting.errors, status: :unprocessable_entity }
    end
  end
end

# DELETE /settings/1
# DELETE /settings/1.json
def destroy
  @setting.destroy
  respond_to do |format|
    format.html { redirect_to settings_url, notice: 'Setting was successfully destroyed.' }
    format.json { head :no_content }
  end
end

private
# Use callbacks to share common setup or constraints between actions.
def set_setting
  @setting = Setting.find(params[:id])
end

# Never trust parameters from the scary internet, only allow the white list through.
def setting_params
  params.require(:setting).permit(:name, :description, :default, :recommended, :bad, :story, :sns)
end
  end

