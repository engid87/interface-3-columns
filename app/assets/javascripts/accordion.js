/**
 * Created by engid87 on 6/4/17.
 */


$('.collapse').on('shown.bs.collapse', function(){
    $(this).parent().find(".fa-plus-square-o").removeClass("fa-plus-square-o").addClass("fa-minus-square-o");
}).on('hidden.bs.collapse', function(){
    $(this).parent().find(".fa-minus-square-o").removeClass("fa-minus-square-o").addClass("fa-plus-square-o");
});
